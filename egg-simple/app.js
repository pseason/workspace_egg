'use strict';

class AppBootHook {
    constructor(app) {
        this.app = app;
    }
    // 配置文件即将加载，这是最后动态修改配置的时机
    configWillLoad(){
        // 此时 config 文件已经被读取并合并，但是还并未生效
        // 这是应用层修改配置的最后时机
        // 注意：此函数只支持同步调用
        // 例如：参数中的密码是加密的，在此处进行解密
        this.app.logger.info('app: %s', 'configWillLoad');
    }
    // 配置文件加载完成
    configDidLoad(){
        this.app.logger.info('app: %s', 'configDidLoad');
    }
    async didLoad() {
        // 所有的配置已经加载完毕
        // 可以用来加载应用自定义的文件，启动自定义的服务
        // 例如：创建自定义应用的示例
        // 例如：加载自定义的目录
        this.app.logger.info('app: %s', 'didLoad');
    }
    // 插件启动完毕
    async willReady(){
        this.app.logger.info('app: %s', 'beforeStart');
        // app.runSchedule 接受一个定时任务文件路径（app/schedule 目录下的相对路径或者完整的绝对路径），执行对应的定时任务，返回一个 Promise
        this.app.runSchedule('schedule_example');
    }
    // worker 准备就绪
    async didReady() {
        // 应用已经启动完毕
        this.app.logger.info('app: %s', 'didReady');
    }
    // 应用启动完成
    async serverDidReady() {
        // http / https server 已启动，开始接受外部请求
        // 此时可以从 app.server 拿到 server 的实例
        this.app.logger.info('app: %s', 'serverDidReady');
    }
    // 应用即将关闭
    async beforeClose() {
        this.app.logger.info('app: %s', 'beforeClose');
    }
};

module.exports = AppBootHook;
