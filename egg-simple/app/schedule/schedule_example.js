'use strict';


const Subscription = require('egg').Subscription;

class Schedule_Example extends Subscription {
    // 通过 schedule 属性来设置定时任务的执行间隔等配置
    static get schedule(){
        return {
            // cron: '0 0 */3 * * *',//每三小时准点执行一次
            interval: '1m', // 1 分钟间隔
            immediate: true,//配置了该参数为 true 时，这个定时任务会在应用启动并 ready 后立刻执行一次这个定时任务。
            type: 'all', // 指定所有的 worker 都需要执行
            // cronOptions: 配置 cron 的时区等
            // disable 配置该参数为 true 时，这个定时任务不会被启动。
            // env: 数组，仅在指定的环境下才启动该定时任务。

            // worker 类型：每台机器上只有一个 worker 会执行这个定时任务，每次执行定时任务的 worker 的选择是随机的。
            // all 类型：每台机器上的每个 worker 都会执行这个定时任务。
        }
    }
    // subscribe 是真正定时任务执行时被运行的函数
    async subscribe(){
        const res = await this.ctx.curl('http://rap2api.taobao.org/app/mock/222914/schedule',{
            dataType: 'json'
        })
        console.log(res.data)
    }
}

module.exports = Schedule_Example;
