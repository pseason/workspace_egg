'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/news', controller.news.list);
  router.get('/user/find/:id', controller.user.find);
  router.get('/user/limit', controller.user.limit);
  router.get('/user/create', controller.user.create);
  router.get('/user/update', controller.user.update);
  router.get('/user/delete/:id', controller.user.delete);

  // 使用 sequelize 遵循 restful 接口风格 post get delete
  router.resources('muser', '/user/model', controller.muser);

};
