'use strict';
const Service = require('egg').Service;

class UserService extends Service {
    async find(id) {
        this.app.logger.info(' select user id: %s',id);
        const user = await this.app.mysql.get('users', { id: id });
        return { user };
    }
    async create(user){
        this.app.logger.info(' create user : {id: %s,name: %s,age: %s}', user.id,user.name,user.age);
        const res = await this.app.mysql.insert('users',user);
        return res;
    }
    async update(user){
        this.app.logger.info(' update user : {id: %s,name: %s,age: %s}',user.id,user.name,user.age);
        const res = await this.app.mysql.update('users',user);
        return res;
    }
    async delete(user){
        this.app.logger.info(' delete user : {id: %s,name: %s,age: %s}',user.id,user.name,user.age);
        const res = await this.app.mysql.delete('users',user);
        return res;
    }
    async limit(){
        const res = await this.app.mysql.select('users',{
            where: { age: [13], name: ['e', 'r'] }, // WHERE 条件
            orders: [['id','desc']], // 排序方式
            limit: 10, // 返回数据量
            offset: 0, // 数据偏移量
        });
        return res;
    }
}

module.exports = UserService;
