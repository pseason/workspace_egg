'use strict';
const Controller = require('egg').Controller;

class UserController extends Controller {
    async find() {
        const ctx = this.ctx;
        const userId = ctx.params.id;
        const user = await ctx.service.user.find(userId);
        ctx.body = user;
        ctx.status = 200;
    }
    async limit(){
        const ctx = this.ctx;
        const users = await ctx.service.user.limit();
        ctx.body = {data: users};
        ctx.status = 200;
    }
    async create() {
        const ctx = this.ctx;
        const user = {
            name: ctx.query.name,
            age: ctx.query.age
        };
        const res = await ctx.service.user.create(user);
        ctx.body = {affectedRows: res.affectedRows};
        ctx.status = 200;
    }
    async update() {
        const ctx = this.ctx;
        const user = {
            id: ctx.query.id,
            name: ctx.query.name,
        };
        const res = await ctx.service.user.update(user);
        ctx.body = {affectedRows: res.affectedRows};
        ctx.status = 200;
    }
    async delete() {
        const ctx = this.ctx;
        const user = {
            id: ctx.params.id,
        };
        const res = await ctx.service.user.delete(user);
        ctx.body = {affectedRows: res.affectedRows};
        ctx.status = 200;
    }
}
module.exports = UserController;
