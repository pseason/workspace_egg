'use strict';
const Controller = require('egg').Controller;

function toInt(str) {
    if (typeof str === 'number') return str;
    if (!str) return str;
    return parseInt(str, 10) || 0;
}

// 定义创建接口的请求参数规则
const createRule = {
    username: 'string',
    age: { type: 'integer', values: [1-120], required: true },
    created: { type: 'string', required: false }
};

class MuserController extends Controller {
    async index() {
        const ctx = this.ctx;
        const query = { limit: toInt(10), offset: toInt(0) };
        ctx.body = await ctx.model.User.findAll(query);
    }
    async show() {
        const ctx = this.ctx;
        ctx.body = await ctx.model.User.findByPk(toInt(ctx.params.id));
    }
    async create() {
        const ctx = this.ctx;
        ctx.validate(createRule, ctx.request.body);
        const { name, age } = ctx.request.body;
        const user = await ctx.model.User.create({ name, age });
        ctx.status = 201;
        ctx.body = user;
    }
    async update() {
        const ctx = this.ctx;
        const id = toInt(ctx.params.id);
        const user = await ctx.model.User.findByPk(id);
        if (!user) {
            ctx.status = 404;
            return;
        }
        const { name, age } = ctx.request.body;
        await user.update({ name, age });
        ctx.body = user;
    }
    async destroy() {
        const ctx = this.ctx;
        const id = toInt(ctx.params.id);
        const user = await ctx.model.User.findByPk(id);
        if (!user) {
            ctx.status = 404;
            return;
        }
        await user.destroy();
        ctx.status = 200;
    }
}


module.exports = MuserController;
