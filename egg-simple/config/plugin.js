'use strict';

/** @type Egg.EggPlugin */

// 单个
// module.exports = {
//     // had enabled by egg
//     static: {
//         enable: true,
//         package: 'egg-view-nunjucks',
//     }
// };

// 启用nunjucks 模板引擎
exports.nunjucks = {
    enable: true,
    package: 'egg-view-nunjucks'
};

// 启用 mysql 插件
exports.mysql = {
    enable: true,
    package: 'egg-mysql'
};

//启用 sequelize 插件
exports.sequelize = {
    enable: true,
    package: 'egg-sequelize',
};

// 启用 validate 插件
exports.validate = {
    enable: true,
    package: 'egg-validate',
};
