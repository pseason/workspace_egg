# egg-simple

### Development

```bash
 npm i
 npm run dev
 指定端口 egg-bin dev --port 7001
 open http://localhost:7001/
```

### Deploy

```bash
 npm start
 npm stop
```

#### 开发步骤
```log
1. 初始化安装
    cnpm install egg --save
    cnpm install egg-bin --save-dev
2.  安装模板
    cnpm install egg-view-nunjucks --save

```

#### 增加 mysql
```log
cnpm install egg-mysql --save
```

#### add sequelize 
```log
1. 
cnpm install --save egg-sequelize mysql2
plugin:
    exports.sequelize = {
      enable: true,
      package: 'egg-sequelize',
    };
```
